# Stage: Build --------------------------------------------------------------- #
FROM node:10 AS build-stage

COPY ./app/ /usr/src/app
RUN cd /usr/src/app && npm install && npm run build

# Stage: Dist ---------------------------------------------------------------- #
FROM nginx:alpine

COPY ./nginx.conf /etc/nginx/nginx.conf

RUN rm -rf /usr/share/nginx/html/*
COPY --from=build-stage /usr/src/app/build/ /usr/share/nginx/html

EXPOSE 4200 80

ENTRYPOINT ["nginx", "-g", "daemon off;"]
