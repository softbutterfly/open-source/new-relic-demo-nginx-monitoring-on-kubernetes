![Sample-Code](https://gitlab.com/softbutterfly/open-source/open-source-office/-/raw/master/banners/softbutterfly-open-source--banner--sample-code.png)

# New Relic Demo: Nginx monitoring on Kubernetes

## Como funciona?

Tenemos una [aplicación frontend en React](https://github.com/bbachi/react-nginx-example.git) que es servida mediante un contenedor de Nginx.

La configuracion de Nginx se encuentra en el archivo [`nginx.conf`](./nginx.conf).

Para el monitoreo de Nginx con kubernetes es importante el siguiente segmento de la configuracion.

```
server {
    listen       8084 default;
    server_name  _;
    server_name_in_redirect  off;

    location /status {
        stub_status on;
        access_log   off;
    }
}
```

Construimos la imagen con el contenedor con el archivo [`Dockerfile`](./Dockerfile) y la publicamos en nuestro registro de contenedores.

En la linea 21 del archivo [`manifest`](./manifest) cambiamos el nombre de la image `CONAINER_IMAGE` y aplicamos el manifiesto.

Luego de seguir las [instrucciones de instalacion de newrelic en kubernetes](https://docs.newrelic.com/docs/kubernetes-pixie/kubernetes-integration/installation/kubernetes-integration-install-configure/) podemos ejecutar el siguiente comando:

```
helm upgrade --reuse-values -f ./values.yaml newrelic-bundle newrelic/nri-bundle
```

Despues de esto el servicio de newrelic reportara las metricas de Ningx.

![Screenshot](./screenshot.png)
